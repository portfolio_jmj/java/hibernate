FROM gradle:8.5.0-jdk21 as BUILD
VOLUME /build
COPY gradle/ /build/gradle
COPY src/ /build
COPY build.gradle /build
COPY gradlew /build
COPY settings.gradle /build
COPY src /build/src
WORKDIR /build
RUN sed -i -e 's/\r$//' gradlew
RUN ./gradlew build

FROM eclipse-temurin:21-jre as run
VOLUME /tmp
COPY --from=BUILD /build/build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]