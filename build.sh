#!/bin/bash

ip=`docker network inspect bridge --format='{{(index .IPAM.Config 0).Gateway}}'`
add="jdbc:postgresql:\/\/$ip:5432\/postgres"

echo "${ip}"

sed -i "s/\(spring.datasource.url=\).*/\1${add}/" ./src/main/resources/application.properties

echo -n "\x1B[36m création de la BDD \x1B[0m"
./Bdd/script.sh

echo -n "\x1B[36m construction de l'image docker \x1B[0m"
docker build -t iolo/springboot1 .

echo -n "\x1B[36m démarrage de l'application \x1B[0m"
docker run --rm -p 8080:8080 iolo/springboot1

